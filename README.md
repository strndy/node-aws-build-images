# node-aws-build-images

Node.js and AWS CLI Docker Image. Good for building and deployin' serverless or FE apps.

Contains:
* Node.Js
* ASW CLI (python based)
* Serverless framework

Can be found here: `docker pull registry.gitlab.com/strndy/node-aws-build-images`

License: [WTFPL](https://en.wikipedia.org/wiki/WTFPL).