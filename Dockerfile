FROM node:10

# Optional stuff to add Terraform and Docker, not needed so far
# RUN apt-get update \
#     && apt-get install -y apt-transport-https software-properties-common build-essential zip unzip python-pip python-dev \
#     && wget https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip \
#     && unzip terraform_0.11.13_linux_amd64.zip \
#     && mv terraform /usr/local/bin \
#     && unlink terraform_0.11.13_linux_amd64.zip \
#     && curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
#     && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
#     && apt-key fingerprint 0EBFCD88 \
#     && apt-get update \
#     && apt-get install -y docker-ce \
#     && rm -rf /var/lib/apt/lists/* \
#     && pip install --upgrade awscli \
#     && npm install -g npm@6 \
#     && npm install -g yarn@1 serverless@1

RUN apt-get update
# RUN apt-get install -y apt-transport-https software-properties-common build-essential zip unzip python-pip python-dev
# RUN pip install --upgrade awscli
RUN apt-get install -y groff less
RUN apt-get install --reinstall groff-base
RUN apt-get install -y awscli
RUN aws --version
RUN aws s3 help | head
RUN npm install -g npm
RUN npm install -g serverless